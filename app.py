from flask import Flask, request, abort
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import *
import json
import requests

app = Flask(__name__)

#   channel access token (line: messaging API)
line_bot_api = LineBotApi('9QfbfQ2JODuHAW1URiEpsa5yVDpHPkFUu1YFußT78PxnnMeMT7emBXBOn+ZWR4f0winfL/0Q0MDCQ9JpmgafjOlSl5+k93EHF+2vrb1KSWmtx50+WfMuJ8QtOtwjFnl/fQDdbWeGyOCYP2yrY4xlMCgdB04t89/1O/w1cDnyilFU=+wWkOlE6bRl3p7IpF3nDV1ttYD/Sply8DuUSYQyzE5B5O24VPQVttQdB04t89/1O/w1cDnyilFU=')
#   channel secret (line: basic settings)
handler = WebhookHandler('f35f22144f7d63958b2bff452fa145a5')

r = requests.get("https://covid19.th-stat.com/api/open/today")

@app.route("/callback", methods=['POST'])
def callback():
    # รับ request ที่ส่งมาในรูปแบบ POST แล้วเก็บในตัวแปร 'req' ในรูปแบบของ JSON
    req = request.get_json(silent=True, force=True)

    #   ดึงค่าต่าง ๆ ที่ dialogflow ส่งมาแบบ POST เก็บค่าเอาไว้ในตัวแปรต่าง ๆ (ตัวอย่าง JSON จาก dialogflow อยู่ที่ไฟล์ ./res.json)
    intent = req["queryResult"]["intent"]["displayName"]
    text = req["originalDetectIntentRequest"]["payload"]["data"]["message"]["text"]
    reply_token = req["originalDetectIntentRequest"]["payload"]["data"]["replyToken"]
    user_id = req["originalDetectIntentRequest"]["payload"]["data"]["source"]["userId"]
    profile = line_bot_api.get_profile(user_id)
    disname = profile.display_name

    # output to console for debug
    print('__'*20)
    print("[disname]:  {}".format(disname))
    print("[text]:  {}".format(text))
    print("[INTENT]:  {}".format(intent))
    print("[reply_token]:  {}".format(reply_token))
    print("[user_id]:  {}".format(user_id))
    # print("[profile]:  {}".format(profile))
    print('__'*20)


    #   ตรวจสอบชื่อของ intent ว่าตรงกันหรือไม่
    if intent == "COVID-19 Situation Reports Intent":

        # เอาข้อมูลที่ได้จากการ requests.get มาแปลงเป็นรูปแบบของ JSON แล้วเก็บลงในตัวแปร data
        data = r.json()

        #   ลูปเพื่อเช็คข้อมูลว่าเป็นค่าติดลบหรือไม่
        for i in data:

            #   เช็คว่าเป็นยอดรวมผู้ติดเชื้อในแต่ละประเภทหรือไม่ (ประเภทยอดรวมพวกนี้ไม่ต้องบอกว่าเป็นค่า + หรือค่า -)
            if  i != "Confirmed" and i != "Recovered" and i != "Hospitalized" and i != "Deaths": 

                #   ถ้าหากข้อมูลเป็น int ให้ เพราะว่ามีข้อมูลประเภทวันที่, เครดิตผู้ทำ และอื่นๆ  ที่เป็น type = str
                if type(data[i]) is int:

                    #   ถ้าหากเป็นค่าที่ติดลบให้แปลงจาก int เป็น str เพื่อสำหรับส่งข้อมูลรูปแบบ JSON
                    if data[i] < 0:
                        print("[-----negative]:{}, {}".format(i, data[i]))
                        data[i] = str(data[i])

                    #   ถ้าหากไม่ใช่ค่าติดลบ ให้เพิ่ม '+' เข้าไปเพื่อความง่ายในการแสดงผล
                    else:
                        print("[positive]:{}, {}".format(i, data[i]))
                        data[i] = "+"+str(data[i])

            #   ถ้าหากเป็น False (ค่ายอดรวมแต่ละประเภท) ให้แปลงจาก int เป็น str เพื่อสำหรับส่งข้อมูล JSON
            else:
                data[i] = str(data[i])
                

        #   ส่งรูปแบบข้อความ 'flex message' กลับไป
        flex_message = FlexSendMessage(
            alt_text='รายงานผู้ติดเชื้อ COVID-19',
            contents={
                "type": "bubble",
                "body": {
                    "type": "box",
                    "layout": "vertical",
                        "contents": [
                            {
                                "type": "text",
                                "text": "รายงานยอดผู้ติดเชื้อ COVID-19",
                                "color": "#0D1B2A",
                                "weight": "bold",
                                "size": "lg",
                                "margin": "xs"
                            },
                            {
                                "type": "text",
                                "text": "ขอบคุณข้อมูลจาก กรมควบคุมโรค",
                                "color": "#98A4AE",
                                "size": "xs",
                                "margin": "xs"
                            },
                            {
                                "type": "separator",
                                "margin": "md"
                            },
                            {
                                "type": "box",
                                "layout": "vertical",
                                "margin": "xxl",
                                "spacing": "sm",
                                "contents": [
                                    {
                                        "type": "box",
                                        "layout": "horizontal",
                                        "contents": [
                                            {
                                                "type": "text",
                                                "size": "md",
                                                "color": "#09225d",
                                                "flex": 0,
                                                "text": "ยอดผู้ติดเชื้อ",
                                                "margin": "none",
                                                "weight": "bold",
                                                "decoration": "none"
                                            },
                                            {
                                                "type": "text",
                                                "text": data["Confirmed"] +" คน (" +data["NewConfirmed"] +")",
                                                "size": "sm",
                                                "color": "#111111",
                                                "align": "end"
                                            }
                                        ]
                                    },
                                    {
                                        "type": "box",
                                        "layout": "horizontal",
                                        "contents": [
                                            {
                                                "type": "text",
                                                "text": "รักษาหายแล้ว",
                                                "size": "md",
                                                "color": "#337357",
                                                "flex": 0,
                                                "weight": "bold"
                                            },
                                            {
                                                "type": "text",
                                                "text": data["Recovered"] +" คน (" +data["NewRecovered"] +")",
                                                "size": "sm",
                                                "color": "#111111",
                                                "align": "end"
                                            }
                                        ]
                                    },
                                    {
                                        "type": "box",
                                        "layout": "horizontal",
                                        "contents": [
                                            {
                                                "type": "text",
                                                "text": "กำลังรักษา",
                                                "size": "md",
                                                "color": "#f18f01",
                                                "flex": 0,
                                                "weight": "bold"
                                            },
                                            {
                                                "type": "text",
                                                "text": data["Hospitalized"] +" คน (" +data["NewHospitalized"] +")",
                                                "size": "sm",
                                                "color": "#111111",
                                                "align": "end"
                                            }
                                        ]
                                    },
                                    {
                                        "type": "box",
                                        "layout": "horizontal",
                                        "margin": "sm",
                                        "contents": [
                                            {
                                                "type": "text",
                                                "text": "ผู้เสียชีวิต",
                                                "color": "#BB0A21",
                                                "weight": "bold",
                                                "size": "md",
                                                "flex": 0
                                            },
                                            {
                                                "type": "text",
                                                "text": data["Deaths"] +" คน (" +data["NewDeaths"] +")",
                                                "size": "sm",
                                                "align": "end"
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "type": "separator",
                                "margin": "xxl"
                            },
                            {
                                "type": "box",
                                "layout": "horizontal",
                                "margin": "lg",
                                "contents": [
                                    {
                                        "type": "text",
                                        "text": "อัพเดทเมื่อ",
                                        "size": "sm",
                                        "color": "#0D1B2A",
                                        "flex": 0
                                    },
                                    {
                                        "type": "text",
                                        "text": data["UpdateDate"] +" น.",
                                        "size": "sm",
                                        "color": "#0D1B2A",
                                        "align": "end"
                                    }
                                ]
                            }
                        ],
                    "backgroundColor": "#ffffff"
                },
                "styles": {
                    "body": {
                        "backgroundColor": "#F0EDEE"
                    },
                    "footer": {
                        "separator": False
                    }
                }
            }
        )

        # ส่งข้อความตอบกลับ (reply message) โดยมี 'reply_token' ที่เอาไว้สำหรับเป็น token ในการตอบกลับ (reply) และข้อความที่ต้องการตอบกลับ คือค่าในตัวแปร 'text_message'
        line_bot_api.reply_message(reply_token, flex_message)
        
    return 'OK'

#   ตรวจเช็ค intent อื่น ๆ ใน dialogflow
    # if intent == "Self-Screening Forms Intent":
    #     carousel_template_message = TemplateSendMessage(
    #         alt_text='Carousel template',
    #         template=CarouselTemplate(
    #             columns=[
    #                 CarouselColumn(
    #                     thumbnail_image_url='https://scdn.line-apps.com/n/channel_devcenter/img/flexsnapshot/clip/clip4.jpg',
    #                     title='คำถามจากโรงพยาบาลราชวิถี',
    #                     text='ทำแบบประเมินความเสี่ยงต่อการติดเชื้อ COVID-19 ด้วยตนเอง',
    #                     actions=[
    #                         # MessageAction(
    #                         #     label='message1',
    #                         #     text='message text1'
    #                         # ),
    #                         URIAction(
    #                             label='ทำแบบประเมิน',
    #                             uri='https://covid19.rajavithi.go.th/test/th_index.php'
    #                         )
    #                     ]
    #                 ),
    #                 CarouselColumn(
    #                     thumbnail_image_url='https://scdn.line-apps.com/n/channel_devcenter/img/flexsnapshot/clip/clip4.jpg',
    #                     title='คำถามจากกรมควบคุมโรค',
    #                     text='ทำแบบประเมินความเสี่ยงต่อการติดเชื้อ COVID-19 ด้วยตนเอง',
    #                     actions=[
    #                         # MessageAction(
    #                         #     label='message2',
    #                         #     text='message text2'
    #                         # ),
    #                         URIAction(
    #                             label='ทำแบบประเมิน',
    #                             uri='https://covid19.th-stat.com/th/self_screening'
    #                         )
    #                     ]
    #                 )
    #             ]
    #         )
    #     )

    #     # line_bot_api.reply_message(reply_token, TextSendMessage(text="เลือกคำถามการประเมินความเสี่ยง"))
    #     line_bot_api.reply_message(reply_token, carousel_template_message)

if __name__ == "__main__":
    app.run()